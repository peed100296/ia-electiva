"""
#Dev: Pablo Escobar
"""

import math
import collections
import os
import time
import random


class Node:
	def __init__(self, puzzle, parent=None, action=None):
		self.puzzle = puzzle
		self.parent = parent
		self.action = action

	def state(self):
		#print(self.puzzle.board)
		return self.puzzle.board
	
	def path(self):
		node, p = self, []
		while node:
			p.append(node.state())
			node = node.parent
		#print(p)
		return p
        	#print(reversed(p))
	
	def solved(self):
		return self.puzzle.solved()

	def actions(self):
		return self.puzzle.actions()
 
class Environment:
	#Inicialización
	def __init__(self, board):
		#Asignaciones
		
		#Extraer la dimensión del board
		nGoal = len(board)
		
		#Crear el goalBoard
		self.goal = [i for i in range(1,nGoal)]
		self.goal.append(0)
		#print("Board Meta: ",self.goal)
		
		self.board = board
		#Como el board es un arreglo, entonces raiz cuadrada(nxn) -> n
		self.width = int(math.sqrt(len(self.board))) 
	

	#Permite copiar elemento por elemento el arreglo perteneciente a la clase y retornar una copia
	def copy(self):
		board = []
		for element in self.board:
			board.append(element)
		return board

	#def moved(self,(r,c),(i,j)):
	def moved(self,tupla1,tupla2):
		#print('(','r,c',')',tupla1)
		#print('(','i,j',')',tupla2)
		r = int(tupla1[0])
		c = int(tupla1[1])
		i = int(tupla2[0])
		j = int(tupla2[1])
		#Copiar matriz original
		copy = self.copy()
		#Realizo intercambio entre dos elementos
		copy[int(r * self.width + c)], copy[int(i * self.width + j)] = copy[int(i * self.width + j)], copy[int(r * self.width + c)] 
		#print("copy:", copy)
		return copy
	
	def solved(self):
		return str(self.board) == str(self.goal)

		
	def actions(self):
		#Posición del espacio en blanco
		block_pos = self.board.index(0)
		print("Posicion del espacio en blanco(0): ", block_pos)
		
		block_r = block_pos / self.width
		block_c = block_pos % self.width
		#print('(','block_r',',','block_c',') = ' ,'(',block_r,",",block_c,')')
		moves = []

		directions = {	"R":(block_r, block_c + 1),
						"L":(block_r, block_c - 1),
						"T":(block_r - 1, block_c),
						"D":(block_r + 1, block_c)}
		for action, (i,j) in directions.items():
			if i >= 0 and j >= 0 and i < self.width and j < self.width:
				#move = self.moved((block_r, block_c),(i,j)) , action
				tupla1 = (block_r, block_c)
				tupla2 = (i,j)
				move = self.moved(tupla1,tupla2),action
				#print("move creado: ", move)
				#os.system("pause")
				moves.append(move)
		return moves
				

	def showboard(self):
		#print("showboard")
		newnode = Node(self.board)
		print(newnode.state()[0])
		return self.board

class Agent:
	def __init__(self, start):
		self.start = start
		#print("agente inicial: ", start)

	def solver(self):
		board = self.start.board
		#Se agrega el nodo inicial a una cola
		fringe = collections.deque([Node(self.start)])
		#print("Node(self.start).state()",Node(self.start).state())
		#print(fringe[0].puzzle.board)
		totalNodos = 0
		#Crea un diccionario llamado visited(visitado)
		visited = set()
		#Agrega al diccionario el primer board
		visited.add(str(fringe[0].state()))
		
		#print("Visited",visited)
		'''k = 0'''
		while fringe:
			'''k = k + 1
			if k > 8:
				break'''
			node = fringe.pop()
			#print("node: ",node, "node.solved",node.solved())
			
			#Verificar si el nodo actual(board) es igual al nodo objetivo(board)
			if node.solved():
				return node.path(), totalNodos
				
			#Si no, entonces ...
			#fila = 0
			for move, action in node.actions():
				#Crea un nodo nuevo
				child = Node(Environment(move), node, action)
				totalNodos = totalNodos + 1
				if action=='R':
					print("Padre:{} \tHijo:{} - accion:{}".format(node.state(),child.state(),"DERECHA"))
				elif action=='L':
					print("Padre:{} \tHijo:{} - accion:{}".format(node.state(),child.state(),"IZQUIERDA"))
				elif action=='U':
					print("Padre:{} \tHijo:{} - accion:{}".format(node.state(),child.state(),"ARRIBA"))
				elif action=='D':
					print("Padre:{} \tHijo:{} - accion:{}".format(node.state(),child.state(),"ABAJO"))
				if str(child.state()) not in visited:
					fringe.appendleft(child)
					visited.add(str(child.state()))
		
		


n = 0
#board = [3,1,0,2]

def process(board):
	inicio = time.time()
	n = len(board)
	p = Environment(board)
	s = Agent(p)
	solved_p, tn = s.solver()
	#print(solved_p)

	nivel = 0
	try: 
		print("\nExiste Solucion?...")
		solucion = []
		for path in reversed(solved_p):
			soln = ''
			for elem in path:
				soln += str(elem)+' '
			#print(soln)
			solucion.append(soln)
			
		#n es la dimensión de la matriz nxn
		for sol in solucion:
			print("\nPaso [",nivel,"]:")
			nivel = nivel + 1
			c = 0
			#print("Lista: ", sol.split())
			
			for i in sol.split(): #Convierto de str a list, para recorrerlo
				if c==n:
					c=0
					print()
				print(i,end=" ")
				c=c+1
		
		print("\nCosto de recorrido: ", nivel-1)
	except TypeError:
		print("\n No tiene solucion...")

		
	final = time.time()
	print("\n\nParametros")
	print("Total Nodos generados {",tn+1,"}.")
	print("Ha tardado {",final - inicio,"} segundos.")
	#print(f"Ha tardado {round(final - inicio, )} segundos.")
	
	#print("Costo de expansion: ", ind)
	#print(p.solved())
	#print(p.actions())

