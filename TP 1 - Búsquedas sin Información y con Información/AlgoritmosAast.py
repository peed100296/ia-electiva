import time
inicio = 0


class Node:
	def __init__(self,data,level,fval):
		""" Initialize the node with the data, level of the node and the calculated fvalue """
		self.data = data
		self.level = level
		self.fval = fval
		self.action = ''
		self.cont = 0
		
	def add_cont():
		self.cont = self.cont + 1
	def get_cont():
		return self.cont
	
	def add_action(self, action):
		self.action = action
	
	def return_action(self):
		if self.action=='L':
			return 'IZQUIERDA'
		elif self.action=='R': 
			return 'DERECHA'
		elif self.action=='U': 
			return 'ARRIBA'
		elif self.action=='D': 
			return 'ABAJO'

	def generate_child(self):
		""" Generate child nodes from the given node by moving the blank space
		either in the four directions {up,down,left,right} """
		#x,y = self.find(self.data,'_')
		x,y = self.find(self.data,0)
		""" val_list contains position values for moving the blank space in either of
		the 4 directions [up,down,left,right] respectively. """
		#val_list = [[x,y-1],[x,y+1],[x-1,y],[x+1,y]]
		val_list = {	"L":[x, y-1],
						"R":[x, y+1],
						"U":[x-1,y],
						"D":[x+1,y]
					}
		
		children = []
		for action, i in val_list.items():
			#print(action,"\t",x,y,"\t",i[0],",",i[1])
			#print(self.data)
			#Envia la tabla, posiciones del cero(x,y) , posiciones de val_list
			#child es un array, action es un carácter
			child = self.shuffle(self.data,x,y,i[0],i[1])
			if child is not None:
				child_node = Node(child,self.level+1,0)
				#Agrego al hijo creado la acción 
				child_node.add_action(action)
				children.append(child_node)
		return children

	def shuffle(self,puz,x1,y1,x2,y2):
		""" Move the blank space in the given direction and if the position value are out
		of limits the return None """
		if x2 >= 0 and x2 < len(self.data) and y2 >= 0 and y2 < len(self.data):
			temp_puz = []
			temp_puz = self.copy(puz)
			temp = temp_puz[x2][y2]
			temp_puz[x2][y2] = temp_puz[x1][y1]
			temp_puz[x1][y1] = temp
			
			return temp_puz
		else:
			return None


	def copy(self,root):
		""" Copy function to create a similar matrix of the given node"""
		temp = []
		for i in root:
			t = []
			for j in i:
				t.append(j)
			temp.append(t)
		return temp    

	def find(self,puz,x):
		""" Specifically used to find the position of the blank space """
		for i in range(0,len(self.data)):
			for j in range(0,len(self.data)):
				if puz[i][j] == x:
					return i,j


class Puzzle:
	def __init__(self,size):
		""" Initialize the puzzle size by the specified size,open and closed lists to empty """
		self.n = size
		self.open = []
		self.closed = []
		self.path = []

	def accept(self):
		""" Accepts the puzzle from the user """
		puz = []
		for i in range(0,self.n):
			temp = input().split(" ")
			puz.append(temp)
			#return [3,1,_,0]
			return puz

			
			
			
	"""
	Porción de código para calcular la heuristica por nro de piezas incorrectas
	"""
	def f1(self,start,goal):
		""" Heuristic Function to calculate heuristic value f(x) = h(x) + g(x) """
		return self.h1(start.data,goal)+start.level

	def h1(self,start,goal):
		""" Calculates the different between the given puzzles """
		temp = 0
		
		#Calculo para encontrar el nro de piezas incorrectas
		for i in range(0,self.n):
			for j in range(0,self.n):
				#Verificación de nro de piezas incorrectas
				#if start[i][j] != goal[i][j] and start[i][j] != '_':
				if start[i][j] != goal[i][j] and start[i][j] != 0:
					temp += 1
		return temp
	
			
	"""
	Porción de código para calcular la heuristica por distancia de Manhattan
	"""
	def f2(self,start,goal):
		""" Heuristic Function to calculate heuristic value f(x) = h(x) + g(x) """
		return self.h2(start.data,goal)+start.level

	def locate_tile(self,tile, grid_state):
		"""Return the co-ordinates of a given tile, given as a tuple.
		Assumes one unique tile in grid."""
		# TODO: should this be a static method: doesn't always operate on self?
		for (y, row) in enumerate(grid_state):
			for (x, value) in enumerate(row):
				if value == tile:
					return (y, x)

		
	def h2(self,state,goal):
		""" Calculates the different between the given puzzles """
		sum = 0
		for (x, row) in enumerate(state):
			#print("x:{}\tfila[array]:{}".format(x,row))
			for (y, tile) in enumerate(row):
				#print("y:{}\tElemento:{}".format(y,tile))
				if tile == 0: #Significa que es el elemento cero(espacio vacío)
					continue
				sum += self.manhattan_distance(tile, (x, y), goal)
		return sum
	
	
	def manhattan_distance(self, tile, tile_position, goal_state):
		"""Calculates the Manhattan distance between a given tile's position
		and its position in goal_state"""
		goal_position = self.locate_tile(tile, goal_state)
		distance = (abs(goal_position[0] - tile_position[0]) 
				   + abs(goal_position[1] - tile_position[1]))
		return distance	
		
	def process(self,start,goal, opt):
		#start = [[3,1],[0,2]]
		#goal  = [[1,2],[3,0]]
		start = Node(start,0,0)
		start.fval1 = self.f1(start,goal)
		self.open.append(start)
		nivel = 0
		totalNodos = 0
		
		#Guardar recorrido
		#self.path.append(self.open[0])
		
		print("\n\n")
		while True:
			cur = self.open[0]
			#Guardar recorrido
			self.path.append(cur)
			
			
			nivel = nivel+1
			print("\nPaso [",nivel,"]:")
			for i in cur.data:
				for j in i:
					print(j,end=" ")
				print("")

			""" If the difference between current and goal node is 0 we have reached the goal node"""
			
			if opt==3:
				if(self.h1(cur.data,goal) == 0):
					break
			elif opt==2:
				if(self.h2(cur.data,goal) == 0):
					break
			
			for i in cur.generate_child():
				totalNodos = totalNodos + 1
				#Mostrar hijos 
				print("Hijo: ",end=" ")
				for m in i.data:
					for k in m:
						print(k,end=" ")
				
				if opt==3: 
					i.fval1 = self.f1(i,goal)
					print("\n[Nro de piezas incorrectas]f1(x) = h(x) + g(x):",i.fval1,"\taccion: ",i.return_action())
				elif opt==2:
					i.fval2 = self.f2(i,goal)
					print("\n[Distancia de Manhattan]f2(x) = h(x) + g(x):",i.fval2,"\taccion: ",i.return_action())
				
				self.open.append(i)
			self.closed.append(cur)
			del self.open[0]

			""" sort the open list based on f value """
			if opt==3:
				self.open.sort(key = lambda x:x.fval1,reverse=False)
			elif opt==2:
				self.open.sort(key = lambda x:x.fval2,reverse=False)
			
			
		#Retorna el recorrido
		return self.path, totalNodos
			

#Dimensiona el N-Puzzle 	
#print("->",N)
#print(start1)
#print(goal1)

def EjecucionAh1(board, goal, N, opt):
#def EjecucionAh1():
	try: 
		inicio = time.time()
		puz = Puzzle(N)
		path,tn = puz.process(board, goal, opt)
		#puz = Puzzle(2)
		#path = puz.process()
		for i in path:
			#if i.return_action() != '' or i.return_action() != 'None':
			print(i.data,"\t",i.return_action())
			#else: 
			#	print(i.data,"\t","Nodo Padre")
	except TypeError:
			print("\n No tiene solucion...")
			
	final = time.time()
	print("\n\nParametros")
	print("Total Nodos generados {",tn+1,"}.")
	print("Ha tardado {",final - inicio,"} segundos.")
	#print(f"Ha tardado {round(final - inicio, 1)} segundos.")
	
	
#EjecucionAh1()