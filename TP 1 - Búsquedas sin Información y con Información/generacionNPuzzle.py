# -*- coding: utf-8 -*-

import random
import time
import os
from BFS import *
from AlgoritmosAast import *

def presentacion():
	seleccion = 0
	while True:
		print("#################################################################")
		print("#################################################################")
		print("##############ALGORITMOS PARA RESOLVER N-PUZZLES#################")
		print("#####   SELECCIONE UN ALGORITMO PARA REALIZAR LA PRUEBA     #####")
		print("#####   1- BFS                                              #####")
		print("#####   2- A* - Distancia de Manhattan                      #####")
		print("#####   3- A* - Nro de Piezas Incorrectas                   #####")
		print("#################################################################")
		print("#################################################################")
		seleccion = int(input("Ingresa una opcion valida: "))
		if seleccion<1 or seleccion>3:
			#Limpiar pantalla
			os.system("clear")
		else:
			break
	return seleccion

def generacionLista():
	n = int(input("Ingresa dimension N del Puzzle (NxN): "))
	inicio = time.time()
	board = [i for i in range(n*n)]
	#print(board)
	for i in range(n*n): 
		li = random.randint(0,n*n-1)
		lf = random.randint(0,n*n-1)
		if li != lf:
			aux = board[lf]
			board[lf] = board[li]
			board[li] = aux

	#Generar lista goal
	goal = [i for i in range(1,n*n)]
	goal.append(0)	

	#print("Lista:",board)
	#print("Meta :",goal)
	#Transformar la lista generada aleatoriamente a Matriz
	mBoard = [] 
	mGoal  = []
	j = 0
	aux1 = []
	aux2 = []
	for i in range(n*n):
		if j==n:
			j=0
			mBoard.append(aux1)
			mGoal.append(aux2)
			aux1 = []
			aux2 = []
		if i==n*n-1:
			mBoard.append(aux1)
			mGoal.append(aux2)
		aux1.append(board[i])
		aux2.append(goal[i])
		j = j+1
			
	final = time.time()
	print(f"\nLa creacion de una matriz generica a tardado {round(final - inicio, 1)} segundos.\n\n")
	return board, mBoard, goal,n
	
def menu():
	#Limpiar pantalla
	os.system("clear")
	seleccion = presentacion()
	
	#Generación de Listas
	x,y,z,N = generacionLista()
	#print(x)
	
	#___________________________
	#para prueba
	"""
	if N==2:
		x = [3,1,0,2]
		y = [[3,1],[0,2]]
		z  = [[1,2],[3,0]]
	elif N==3:
		x = [2,4,3,1,0,6,7,5,8]
		y = [[2,4,3],[1,0,6],[7,5,8]]
		z = [[1,2,3],[4,5,6],[7,8,0]]
	elif N==4:
		x = [5,1,3,0,4,11,8,7,9,10,2,13,15,6,14,12]
		y = [[5,1,3,0],[4,11,8,7],[9,10,2,13],[15,6,14,12]]
		z = [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,0]]
		#
		#x = [5,7,8,11,9,4,3,1,15,10,2,13,6,14,12,0]
		#y = [[5,7,8,11],[9,4,3,1],[15,10,2,13],[6,14,12,0]]
		#_________________________
	
	elif N==5:
		#y = [[1, 19, 9, 4, 0], [13, 15, 10, 6, 24], [16, 3, 23, 21, 8], [14, 18, 12, 5, 7], [17, 2, 22, 11, 20]]
		x = [9,10,11,12,13,8,21,22,23,14,7,20,0,24,15,6,19,18,17,16,5,4,3,2,1]
		y = [[9,10,11,12,13],[8,21,22,23,14],[7,20,0,24,15],[6,19,18,17,16],[5,4,3,2,1]]
		z = [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15],[16,17,18,19,20],[21,22,23,24,0]]
	"""
	
	#Mostar visualmente la matriz creada
	print("\nPuzzle Inicial")
	for i in range(N):
			print(y[i])
			
	print("\nPuzzle Objetivo")
	for i in range(N):
			print(z[i])
	
	#Invocar funcion para generar matriz nxn generica
	if seleccion == 1:
		#Invocar a DFS
		print("BFS")
		process(x)
	elif seleccion == 2: 
		#Invocar a A* - Distancia de Manhattan
		print("A* - Distancia de Manhattan")
		EjecucionAh1(y,z,N,seleccion)
	else:
		#Invocar a A* - Nro de Piezas Incorrectas 
		print("A* - Nro de Piezas Incorrectas ")
		EjecucionAh1(y,z,N,seleccion)
		
while True:
	os.system("clear")		
	menu()







