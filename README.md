# Integrantes
1- Pablo Escobar
2- Melissa Bogado
3- Adrián Vera
4- Romina Rotela



# IA - ELECTIVA - TP 1 - ALGORITMOS SIN Y CON INFORMACIÓN APLICADA AL N-PUZZLE

_En este proyecto se anexará todos los TPs realizados en el curso._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Tener instalado python_

```
python version 3.7.6
```

### Instalación 🔧

_Descargar el repositorio en el local_

_Ejecutar el script generacionNPuzzle.py_

```
python generacionNPuzzle.py
```

## Ejecutando las pruebas ⚙️

_Ingresar una opción válida para probar uno de los 3 algoritmos propuestos_
1. BFS
2. A* -  Distancia de Manhattan
3. A* - Nro de Piezas Incorrectas

### Analice las pruebas end-to-end 🔩

_El objetivo de esta prueba es comprobar los costos de ejecución de los sgtes algoritmos para la resolucción del problema de N-PUZZLE_


### Y las pruebas de estilo de codificación ⌨️

_A) ALGORITMO BFS_


_B) ALGORITMO A* -  Distancia de Manhattan_
_A parte del algoritmo de BFS, luego de generar los hijos, verifica lo sgte:_
```
 Ejemplos
 board = [3 1]
         [0 2]
         
 goal  = [1 2]
         [3 0]  
```
1. Se invoca a la función f2()
    - Recibe por parámetro la tabla a validar(board) y la tabla meta(board goal) 
    - Llama a la función h2(), que se encargará de calcular la distancia de Manhattan
2. La función h2()
    - Recibe por parámetro la tabla a validar(board) y la tabla meta(board goal) 
    - Realiza un bucle for para extraer la posición de la fila(x), y el array de dicha fila.
        Ejemplo:    x|fila(x)
                ---------------
                    0|[3 1] 
                    1|[0 2]
    - Luego recorre cada fila(x), para extraer cada elemento, y así tener el valor de la posición en y
    - Compara que cada elemento sea distinto a cero(espacio vacío), para enviar a la función manhattan_distance() 
3.  La función manhattan_distance()
    - Recibe por parámetro el elemento del puzzle(board) que sea distinto a cero, además de su posición.  Y por último, el puzzle goal.
    - Invoca a la función locate_tile() y le pasa por parámetro el elemento distinto a cero y el puzzle goal, para que le retorne la posición del mismo elemento pero en el tablero goal.
    - Luego, calcula la distancia de manhattan entre el par ordenado(x1,y1) del elemento en el board y el par ordenado(x2,y2) del mismo elemento pero en el goal.
    distancia = abs(x2-x1)+abs(y2-y1)
    - Por último, retorna de nuevo a la función f2(), y posterior a eso retorna el valor de la distancia sumandole al nivel del arbol.
    f2(x) = g(x) + h2(x)
                    
   
```

	def f2(self,start,goal):
		""" Heuristic Function to calculate heuristic value f(x) = h(x) + g(x) """
		return self.h2(start.data,goal)+start.level

	def locate_tile(self,tile, grid_state):
		Assumes one unique tile in grid."""
		# TODO: should this be a static method: doesn't always operate on self?
		for (y, row) in enumerate(grid_state):
			for (x, value) in enumerate(row):
				if value == tile:
					return (y, x)

	def h2(self,state,goal):
		""" Calculates the different between the given puzzles """
		sum = 0
		for (x, row) in enumerate(state):
			#print("x:{}\tfila[array]:{}".format(x,row))
			for (y, tile) in enumerate(row):
				#print("y:{}\tElemento:{}".format(y,tile))
				if tile == 0: #Significa que es el elemento cero(espacio vacío)
					continue
				sum += self.manhattan_distance(tile, (x, y), goal)
		return sum
	
	
	def manhattan_distance(self, tile, tile_position, goal_state):
		"""Calculates the Manhattan distance between a given tile's position
		and its position in goal_state"""
		goal_position = self.locate_tile(tile, goal_state)
		distance = (abs(goal_position[0] - tile_position[0]) 
				   + abs(goal_position[1] - tile_position[1]))
		return distance	
        
        .
        .
        .
        .
        .
        .
        .

Obs. Luego de obtener los hijos y sus pesos correspondiente, aplico:
    - self.open.sort(key = lambda x:x.fval2,reverse=False) 
    para ordenar por menor peso, y así seleccionar la rama a trabajar, y por lo tanto descartar el resto.
```


_3) A* - Nro de Piezas Incorrectas_
A parte del algoritmo de BFS, luego de generar los hijos, verifica lo sgte:
``` 
 Ejemplos
 board = [3 1]
         [0 2]
         
 goal  = [1 2]
         [3 0]         
```
1. Se invoca a la función f1()
    - Recibe por parámetro la tabla a validar(board) y la tabla meta(board goal) 
    - Llama a la función h1(), que se encargará de calcular Nro de Piezas Incorrectas
2. La función h1()
    - Recibe por parámetro la tabla a validar(board) y la tabla meta(board goal) 
    - Realiza dos bucles para extraer los elementos del board y goal, y compararlos.
    ```
    Ejemplo:    pos  |  board | goal | Son distintos? | el elemento del board es cero? | temp++
                -----------------------------------------------------------------------|-----
                (0,0)|     3  |  1   |       SI       |                NO              |   1
                (0,1)|     1  |  2   |       SI       |                NO              |   2
                (1,0)|     0  |  3   |       SI       |                SI              |   -
                (1,1)|     2  |  0   |       SI       |                SI              |   3
    ```                
    Obs. Por lo tanto, retorna el valor de 3 como peso de h(x)
    - Por último, retorna de nuevo a la función f1(), y posterior a eso retorna el valor de nro de piezas distintas sumandole al nivel del arbol.
    f1(x) = g(x) + h1(x)
                    
                    
```
def f1(self,start,goal):
		return self.h1(start.data,goal)+start.level

	def h1(self,start,goal):
		temp = 0
		
		#Calculo para encontrar el nro de piezas incorrectas
		for i in range(0,self.n):
			for j in range(0,self.n):
				#Verificación de nro de piezas incorrectas
				if start[i][j] != goal[i][j] and start[i][j] != 0:
					temp += 1
		return temp
        
        
        .
        .
        .
        .
        
Obs. Luego de obtener los hijos y sus pesos correspondiente, aplico:
    - self.open.sort(key = lambda x:x.fval1,reverse=False) 
    para ordenar por menor peso, y así seleccionar la rama a trabajar, y por lo tanto descartar el resto.
```



